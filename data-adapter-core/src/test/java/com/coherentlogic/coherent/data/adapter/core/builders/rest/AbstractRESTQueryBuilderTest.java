package com.coherentlogic.coherent.data.adapter.core.builders.rest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.coherentlogic.coherent.data.adapter.core.cache.CacheServiceProviderSpecification;
import com.coherentlogic.coherent.data.adapter.core.command.CommandExecutorSpecification;
import com.coherentlogic.coherent.data.adapter.core.command.MultithreadedCommandExecutor;
import com.coherentlogic.coherent.data.adapter.core.exceptions.ConversionFailedException;
import com.coherentlogic.coherent.data.adapter.core.listeners.QueryBuilderEvent.EventType;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;

/**
 * Unit test for the {@link AbstractRESTQueryBuilder} class.
 *
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class AbstractRESTQueryBuilderTest {

    private static final String TEST = "test",
        TEST_COM = "http://www.test.com/";

    private AbstractRESTQueryBuilder<String> queryBuilder = null;

    @BeforeEach
    public void setUp() throws Exception {
        queryBuilder = new ExampleRESTQueryBuilder (null, TEST_COM);
    }

    @AfterEach
    public void tearDown() throws Exception {
        queryBuilder = null;
    }

    @Test
    public void addParameterListWithSeparator () {

        assertThrows (
            NullPointerException.class,
            () -> {
                List<String> values = null;

                queryBuilder.addParameter("nullTest", "*", values);
            }
        );
    }

    @Test
    public void addParameterListOfSizeOneWithSeparator () {

        List<String> values = Arrays.asList("bar");

        queryBuilder.addParameter("foo", "*", values);

        assertEquals("http://www.test.com/?foo=bar", queryBuilder.getEscapedURI());
    }

    @Test
    public void addParameterListOfSizeManyWithSeparator () {

        List<String> values = Arrays.asList("bar", "baz", "boo");

        queryBuilder.addParameter("foo", "*", values);

        assertEquals("http://www.test.com/?foo=bar*baz*boo", queryBuilder.getEscapedURI());
    }

    @Test
    public void testReplacePathUsingAnArray () {

        String escapedURI = queryBuilder.replacePath("foo/${0}/bar/${1}", "foo", "bar").getEscapedURI();

        assertEquals("http://www.test.com/foo/foo/bar/bar", escapedURI);
    }

    @Test
    public void testReplaceQueryUsingAnArray () {

        String escapedURI = queryBuilder.replaceQuery("bar=${0}", "bar").getEscapedURI();

        assertEquals("http://www.test.com/?bar=bar", escapedURI);
    }

    @Test
    public void testReplacePathUsingAMap () {

        Map<String, Object> values = new HashMap<String, Object> ();

        values.put("a", new Integer (123));
        values.put("b", "bar");
        values.put("c", "baz");

        String escapedURI =
            queryBuilder.replacePath("foo/${a}/bar/${b}/baz/${c}", values).getEscapedURI();

        assertEquals("http://www.test.com/foo/123/bar/bar/baz/baz", escapedURI);
    }

    @Test
    public void testFragment () {

        String escapedURI = queryBuilder.fragment("boo").getEscapedURI();

        assertEquals("http://www.test.com/#boo", escapedURI);
    }

    /**
     * @todo Test for when an exception is thrown.
     */
    @Test
    public void testQueryBuilderEventListener() {

        AtomicLong ctr = new AtomicLong ();

        queryBuilder.addQueryBuilderEventListener(
            event -> {
                ctr.incrementAndGet();
            }
        );

        queryBuilder.doGet(SerializableBean.class);

        assertEquals(4, ctr.get());
    }

    @Test
    public void testAddParameterUsingDate () {

        SimpleDateFormat dateFormat = new SimpleDateFormat ("yyyy-MM-dd");

        Calendar testCalendar = Calendar.getInstance();

        testCalendar.clear();

        testCalendar.set(1992, Calendar.JULY, 20);

        queryBuilder.addParameter("testDate", dateFormat, testCalendar.getTime());

        assertEquals("http://www.test.com/?testDate=1992-07-20", queryBuilder.getEscapedURI());
    }

    @Test
    public void testPutWithNullName() {

        assertThrows (
            NullPointerException.class,
            () -> {
                queryBuilder.addParameter(null, TEST);
            }
        );
    }

    @Test
    public void testPutWithNullValue1() {

        assertThrows (
            NullPointerException.class,
            () -> {
                queryBuilder.addParameter(TEST, (String) null);
            }
        );
    }

    @Test
    public void testPutWithNullValue2() {

        assertThrows (
            NullPointerException.class,
            () -> {
                queryBuilder.addParameter(TEST, (BigDecimal) null);
            }
        );
    }

    @Test
    public void testPutWithValidValues() {
        queryBuilder.addParameter(TEST, TEST);

        String escapedUri = queryBuilder.getEscapedURI();

        assertEquals ("http://www.test.com/?test=test", escapedUri);
    }

    @Test
    public void testCacheFunctionalityWhereObjectHasBeenCached () {

        CacheServiceProviderSpecification<String> cache =
            new CacheServiceProvider();

        ExampleRESTQueryBuilder testQueryBuilder =
            new ExampleRESTQueryBuilder(null, TEST_COM, cache);

        testQueryBuilder
            .foo()
            .setBar(ExampleRESTQueryBuilder.BAZ);

        String escapedURI = testQueryBuilder.getEscapedURI();

        SerializableBean expected = new SerializableBean ();

        cache.put(escapedURI, expected);

        SerializableBean actual = testQueryBuilder.doGet(SerializableBean.class);

        assertEquals(expected, actual);
    }

    /**
     * This test uses a DefaultCommandExecutor and a simple local cache.
     */
    @Test
    public void testCacheFunctionalityWhereObjectHasBeenCached2 () {

        CacheServiceProviderSpecification<String> cache =
            new CacheServiceProvider();

        ExampleRESTQueryBuilder testQueryBuilder =
            new ExampleRESTQueryBuilder(null, TEST_COM, cache, new MultithreadedCommandExecutor(3));

        testQueryBuilder
            .foo()
            .setBar(ExampleRESTQueryBuilder.BAZ);

        String escapedURI = testQueryBuilder.getEscapedURI();

        SerializableBean expected = new SerializableBean ();

        cache.put(escapedURI, expected);

        AtomicInteger ai = new AtomicInteger (0);

        testQueryBuilder.addQueryBuilderEventListener(event -> {

            if (event.getEventType() == EventType.methodBegins
                || event.getEventType() == EventType.preCacheCheck
                || event.getEventType() == EventType.cacheHit
                || event.getEventType() == EventType.methodEnds) {

                ai.incrementAndGet();

                // System.out.println("event.getEventType: " + event.getEventType());

            } else
                throw new RuntimeException("Unexpected event: " + event.getEventType());
        });

        SerializableBean actual = testQueryBuilder.doGet(SerializableBean.class);

        assertEquals(new Integer (4), (Integer) ai.get());
        assertEquals(expected, actual);
    }
}

class CacheServiceProvider implements CacheServiceProviderSpecification<String> {

    private final Map<String, SerializableBean> cache = new HashMap<String, SerializableBean> ();

    @Override
    public <V extends SerializableBean> V get(String key) {
        // Suspect
        return (V) cache.get(key);
    }

    @Override
    public <V extends SerializableBean> void put(String key, V value) {
        cache.put(key, value);
    }
}

//class TestQueryBuilder extends AbstractRESTQueryBuilder<String> {
//
//    public static final String FOO = "foo", BAR = "bar", BAZ = "baz";
//
//    protected TestQueryBuilder(RestTemplate restTemplate, String uri) {
//        super (restTemplate, uri);
//    }
//
//    public TestQueryBuilder(
//        RestTemplate restTemplate,
//        String uri,
//        CacheServiceProviderSpecification<String> cache,
//        CommandExecutorSpecification<String> commandExecutor
//    ) {
//        super(restTemplate, uri, cache, commandExecutor);
//    }
//
//    protected TestQueryBuilder(
//        RestTemplate restTemplate,
//        String uri,
//        CacheServiceProviderSpecification<String> cache
//    ) {
//        super(restTemplate, uri, cache);
//    }
//
//    /**
//     * Extends the path to include 'foo' -- for example:
//     *
//     * http://www.foobar.zzz/foo/
//     */
//    public TestQueryBuilder foo () {
//
//        extendPathWith(FOO);
//
//        return this;
//    }
//
//    /**
//     * Setter method for the bar parameter.
//     */
//    public TestQueryBuilder setBar (String bar) {
//
//        addParameter(BAR, bar);
//
//        return this;
//    }
//
//    @Override
//    public String getCacheKey() {
//        return getEscapedURI();
//    }
//
//    @Override
//    protected <T> T doExecute(Class<T> type) {
//        return null;
//    }
//}