package com.coherentlogic.coherent.data.adapter.core.exceptions;

import org.springframework.core.NestedRuntimeException;

public class IllegalValueRuntimeException extends NestedRuntimeException {

    private static final long serialVersionUID = 6433103971341264819L;

    public IllegalValueRuntimeException(String msg) {
        super(msg);
    }
}
