package com.coherentlogic.coherent.data.adapter.core.plugins;

import java.util.Optional;

import org.springframework.beans.factory.config.BeanDefinition;

public class DefaultPlugin implements PluginSpecification {

    private final String pluginName;

    private String version, author, emailAddress, description, documentation;

    private final BeanDefinition beanDefinition;

    public DefaultPlugin(String pluginName, BeanDefinition beanDefinition) {
        super();
        this.pluginName = pluginName;
        this.beanDefinition = beanDefinition;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDocumentation(String documentation) {
        this.documentation = documentation;
    }

    @Override
    public Optional<String> getVersion() {
        return Optional.ofNullable(version);
    }

    @Override
    public Optional<String> getAuthor() {
        return Optional.ofNullable(author);
    }

    @Override
    public Optional<String> getEmailAddress() {
        return Optional.ofNullable(emailAddress);
    }

    @Override
    public Optional<String> getPluginName() {
        return Optional.of(pluginName);
    }

    @Override
    public Optional<String> getDescription() {
        return Optional.ofNullable(description);
    }

    @Override
    public Optional<String> getDocumentation() {
        return Optional.ofNullable(documentation);
    }

    @Override
    public Optional<BeanDefinition> getBeanDefinition() {
        return Optional.of(beanDefinition);
    }

    @Override
    public String toString() {
        return "AbstractPlugin [version=" + version + ", pluginName=" + pluginName + ", author=" + author
            + ", emailAddress=" + emailAddress + ", plugingName=" + pluginName + ", description=" + description +
            ", documentation=" + documentation + ", beanDefinition=" + beanDefinition + "]";
    }
}
