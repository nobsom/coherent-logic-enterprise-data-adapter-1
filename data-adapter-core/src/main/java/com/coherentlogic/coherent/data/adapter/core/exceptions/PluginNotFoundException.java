package com.coherentlogic.coherent.data.adapter.core.exceptions;

import org.springframework.core.NestedRuntimeException;

/**
 * 
 * @author Thomas P. Fuller
 */
public class PluginNotFoundException extends NestedRuntimeException {

	private static final long serialVersionUID = 3063821567377590614L;

	public PluginNotFoundException(String msg) {
		super(msg);
	}

	public PluginNotFoundException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
