package com.coherentlogic.coherent.data.adapter.core.plugins;

/**
 *
 * 
 */
public interface PluginContainerSpecification {

	<T> T lookup (String name);

	<T> T lookup (Class<?> type);
}
