package com.coherentlogic.coherent.data.adapter.core.builders;

import java.util.ArrayList;
import java.util.List;

import com.coherentlogic.coherent.data.adapter.core.cache.CacheServiceProviderSpecification;
import com.coherentlogic.coherent.data.adapter.core.cache.NullCache;
import com.coherentlogic.coherent.data.adapter.core.command.CommandExecutorSpecification;
import com.coherentlogic.coherent.data.adapter.core.command.DefaultCommandExecutor;
import com.coherentlogic.coherent.data.adapter.core.listeners.QueryBuilderEventListener;

/**
 * Apply the JAMON Performance Interceptor as follows:
 *
 * <pre>
 * <bean id="springMonitoringAspectInterceptor"
 *  class="org.springframework.aop.interceptor.JamonPerformanceMonitorInterceptor">
 *     <property name="trackAllInvocations" value="true"/>
 *     <property name="useDynamicLogger" value="true"/>
 * </bean>
 *
 * <aop:config proxy-target-class="true">
 *     <aop:advisor advice-ref="springMonitoringAspectInterceptor"
 *      pointcut=
 *      "execution(public * com.coherentlogic.coherent.data.model.core.builders.HttpMethodsSpecification.doGet(..)))"/>
 * </aop:config>
 * </pre>
 *
 * The important item to note here is that the pointcut is applied to the </b>interface<b> and not the implementation --
 * if you target the implementation nothing will appear in the log.
 *
 * Also note that whatever bean you're applying the interceptor to <b>must</b> be obtained through the Spring container,
 * otherwise the interceptor will not be applied to it.
 *
 * @todo Auto-apply the performance interceptor.
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public abstract class AbstractCacheableQueryBuilder<K> extends AbstractQueryBuilder<K> {

    private final CacheServiceProviderSpecification<K> cache;

    public AbstractCacheableQueryBuilder() {
        this(new NullCache<K> (), new DefaultCommandExecutor<K> ());
    }

    public AbstractCacheableQueryBuilder(
        CacheServiceProviderSpecification<K> cache
    ) {
        this(cache, new DefaultCommandExecutor<K> ());
    }

    public AbstractCacheableQueryBuilder(CommandExecutorSpecification<K> commandExecutor) {
        this (new NullCache<K> (), new ArrayList<QueryBuilderEventListener<K>> (), commandExecutor);
    }

    public AbstractCacheableQueryBuilder(
        CacheServiceProviderSpecification<K> cache,
        CommandExecutorSpecification<K> commandExecutor
    ) {
        this (cache, new ArrayList<QueryBuilderEventListener<K>> (), commandExecutor);
    }

    public AbstractCacheableQueryBuilder(
        CacheServiceProviderSpecification<K> cache, 
        List<QueryBuilderEventListener<K>> queryBuilderListeners,
        CommandExecutorSpecification<K> commandExecutor
    ) {
        super(queryBuilderListeners, commandExecutor);

        this.cache = cache;
    }

    public CacheServiceProviderSpecification<K> getCache() {
        return cache;
    }

    @Override
    public String toString() {
        return "CacheableQueryBuilder [cache=" + cache + ", toString()=" + super.toString() + "]";
    }
}
