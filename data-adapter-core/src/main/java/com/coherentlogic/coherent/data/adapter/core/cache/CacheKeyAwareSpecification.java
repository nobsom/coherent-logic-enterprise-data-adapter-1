package com.coherentlogic.coherent.data.adapter.core.cache;

import com.coherentlogic.coherent.data.adapter.core.builders.rest.AbstractRESTQueryBuilder;

/**
 * A specification for classes which have cache key awareness.
 *
 * Method is used to get the key for this instance of the {@link AbstractRESTQueryBuilder} -- the key is then used to
 * lookup the value in the cache. If the value exists then it is returned to the user, otherwise the method should
 * perform the load operation (be loading from a file or from a call to a web service, etc).
 *
 * @param <K> The type of cache key in use -- as a general rule, this should be serializable.
 */
public interface CacheKeyAwareSpecification<K> {

    /**
     * @return An instance of the cacheKey.
     */
    K getCacheKey ();
}
