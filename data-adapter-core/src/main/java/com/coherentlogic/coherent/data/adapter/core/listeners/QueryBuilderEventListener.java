package com.coherentlogic.coherent.data.adapter.core.listeners;

import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;

/**
 * A listener for events that are fired by the {@link QueryBuilder}.
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 *
 * @param <K> The key type.
 */
@FunctionalInterface
public interface QueryBuilderEventListener<K> {

    void onEvent (QueryBuilderEvent<K, ? extends SerializableBean> event);
}
