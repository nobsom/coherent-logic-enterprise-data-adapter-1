package com.coherentlogic.coherent.data.adapter.core.builders.csv;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.junit.jupiter.api.Test;

import com.coherentlogic.coherent.data.adapter.core.domain.csv.DefaultResultBean;
import com.coherentlogic.coherent.data.adapter.core.exceptions.IORuntimeException;

/**
 * Unit test for the {@link AbstractCSVQueryBuilder} class.
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class AbstractCSVQueryBuilderTest {

    /**
     * Test data is acquired from https://fred.stlouisfed.org/series/sp500.
     */
    @Test
    void test() throws IOException, URISyntaxException {

        ClassLoader classLoader = getClass().getClassLoader();

        File file = new File(classLoader.getResource("FRED-SERIES-SP500.csv").toURI());

        if (!file.exists())
            throw new FileNotFoundException("Unable to locate the FRED-SERIES-SP500.csv test file; file.absolutePath: "
                + file.getAbsolutePath());

        Reader reader = new FileReader(file);//"/src/test/resources/FRED-SERIES-SP500.csv");

        CSVParser csvParser = new CSVParser (
            reader,
            CSVFormat.newFormat(',')
                .withFirstRecordAsHeader()
                .withQuote('"')
                .withRecordSeparator("\r\n")
                .withIgnoreEmptyLines(true)
        );

        SP500CSVQueryBuilder builder = new SP500CSVQueryBuilder (csvParser);

        DefaultResultBean result = builder.doGet(DefaultResultBean.class);

        System.out.println("result: " + result);

        assertEquals(2, result.getHeaders().size());
    }

}

class SP500CSVQueryBuilder extends AbstractCSVQueryBuilder<DefaultResultBean> {

    public SP500CSVQueryBuilder(CSVParser csvParser) {
        super(csvParser);
    }

    @Override
    protected Object doExecute(Class type) {

        final DefaultResultBean result = new DefaultResultBean ();

        getCSVParser()
            .getHeaderMap()
            .keySet()
            .forEach(
                key -> { result.addHeader(key); }
            );

        try {
            getCSVParser()
                .getRecords()
                .forEach(
                    record -> {

                        final List<String> nextContentRow = new ArrayList<String> (record.size());

                        record.forEach(
                            entry -> {
                                nextContentRow.add(entry);
                            }
                        );

                        result.addContent(nextContentRow);
                    }
                );
        } catch (IOException e) {
            throw new IORuntimeException();
        }

        return result;
    }
}