package com.coherentlogic.coherent.data.model.core.domain.security;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.domain.IdentitySpecification;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;

/**
 * 
 */
@Entity
@Table(name = GrantedAuthorityBean.GRANTED_AUTHORITY_TABLE)
public class GrantedAuthorityBean extends SerializableBean
    implements GrantedAuthority, IdentitySpecification<Long> {

    private static final long serialVersionUID = -1561652711867852682L;

    static final String AUTHORITY = "authority", GRANTED_AUTHORITY_TABLE = "grantedAuthority";

    private Long id;

    private String authority;

    @Override
    public String getAuthority() {
        return authority;
    }

    public void setAuthority(@Changeable (AUTHORITY) String authority) {
        this.authority = authority;
    }

    @Override
    public void setId(@Changeable (ID) Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((authority == null) ? 0 : authority.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        GrantedAuthorityBean other = (GrantedAuthorityBean) obj;
        if (authority == null) {
            if (other.authority != null)
                return false;
        } else if (!authority.equals(other.authority))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "GrantedAuthorityBean [id=" + id + ", authority=" + authority + "]";
    }
}
