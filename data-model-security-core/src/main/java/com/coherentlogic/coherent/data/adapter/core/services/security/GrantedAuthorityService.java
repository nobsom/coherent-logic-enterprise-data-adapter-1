package com.coherentlogic.coherent.data.adapter.core.services.security;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import com.coherentlogic.coherent.data.adapter.core.repositories.security.GrantedAuthorityRepository;
import com.coherentlogic.coherent.data.model.core.domain.security.GrantedAuthorityBean;

@Repository(GrantedAuthorityService.BEAN_NAME)
@Transactional
public class GrantedAuthorityService {

    static final String BEAN_NAME = "grantedAuthorityService";

    @Autowired
    private GrantedAuthorityRepository grantedAuthorityRepository;

    public long count() {
        return grantedAuthorityRepository.count();
    }

    public <S extends GrantedAuthorityBean> long count(Example<S> arg0) {
        return grantedAuthorityRepository.count(arg0);
    }

    public void delete(Long arg0) {
        grantedAuthorityRepository.deleteById(arg0);
    }

    public void delete(GrantedAuthorityBean arg0) {
        grantedAuthorityRepository.delete(arg0);
    }

    public void delete(Iterable<? extends GrantedAuthorityBean> arg0) {
        grantedAuthorityRepository.deleteAll(arg0);
    }

    public void deleteAll() {
        grantedAuthorityRepository.deleteAll();
    }

    public void deleteAllInBatch() {
        grantedAuthorityRepository.deleteAllInBatch();
    }

    public void deleteInBatch(Iterable<GrantedAuthorityBean> arg0) {
        grantedAuthorityRepository.deleteInBatch(arg0);
    }

    public boolean exists(Long arg0) {
        return grantedAuthorityRepository.existsById(arg0);
    }

    public <S extends GrantedAuthorityBean> boolean exists(Example<S> arg0) {
        return grantedAuthorityRepository.exists(arg0);
    }

    public List<GrantedAuthorityBean> findAll() {
        return grantedAuthorityRepository.findAll();
    }

    public List<GrantedAuthorityBean> findAll(Sort arg0) {
        return grantedAuthorityRepository.findAll(arg0);
    }

    public List<GrantedAuthorityBean> findAll(Iterable<Long> arg0) {
        return grantedAuthorityRepository.findAllById(arg0);
    }

    public <S extends GrantedAuthorityBean> List<S> findAll(Example<S> arg0) {
        return grantedAuthorityRepository.findAll(arg0);
    }

    public <S extends GrantedAuthorityBean> List<S> findAll(Example<S> arg0, Sort arg1) {
        return grantedAuthorityRepository.findAll(arg0, arg1);
    }

    public Page<GrantedAuthorityBean> findAll(Pageable arg0) {
        return grantedAuthorityRepository.findAll(arg0);
    }

    public <S extends GrantedAuthorityBean> Page<S> findAll(Example<S> arg0, Pageable arg1) {
        return grantedAuthorityRepository.findAll(arg0, arg1);
    }

    public GrantedAuthorityBean findOne(Long arg0) {
        // This needs to be tested, note use of get.
        return grantedAuthorityRepository.findById(arg0).get();
    }

    public <S extends GrantedAuthorityBean> S findOne(Example<S> arg0) {
        // This needs to be tested, note use of get.
        return grantedAuthorityRepository.findOne(arg0).get();
    }

    public void flush() {
        grantedAuthorityRepository.flush();
    }

    public GrantedAuthorityBean getOne(Long arg0) {
        return grantedAuthorityRepository.getOne(arg0);
    }

    public <S extends GrantedAuthorityBean> List<S> save(Iterable<S> arg0) {
        return grantedAuthorityRepository.saveAll(arg0);
    }

    public <S extends GrantedAuthorityBean> S save(S arg0) {
        return grantedAuthorityRepository.save(arg0);
    }

    public <S extends GrantedAuthorityBean> S saveAndFlush(S arg0) {
        return grantedAuthorityRepository.saveAndFlush(arg0);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((grantedAuthorityRepository == null) ? 0 :
            grantedAuthorityRepository.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        GrantedAuthorityService other = (GrantedAuthorityService) obj;
        if (grantedAuthorityRepository == null) {
            if (other.grantedAuthorityRepository != null)
                return false;
        } else if (!grantedAuthorityRepository.equals(other.grantedAuthorityRepository))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "GrantedAuthorityService [grantedAuthorityRepository=" + grantedAuthorityRepository + "]";
    }
}
