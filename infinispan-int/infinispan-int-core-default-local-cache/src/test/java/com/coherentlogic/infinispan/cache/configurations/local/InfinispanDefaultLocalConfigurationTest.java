package com.coherentlogic.infinispan.cache.configurations.local;

import org.infinispan.manager.EmbeddedCacheManager;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class InfinispanDefaultLocalConfigurationTest {

    @Test
    public void testGetCacheManager () {

        InfinispanDefaultLocalConfiguration configuration = new InfinispanDefaultLocalConfiguration ();

        EmbeddedCacheManager ecm = configuration.getCacheManager();

        ecm.getCache().put("foo", "bar");

        String bar = (String) ecm.getCache().get("foo");

        assertEquals("bar", bar);
    }
}
