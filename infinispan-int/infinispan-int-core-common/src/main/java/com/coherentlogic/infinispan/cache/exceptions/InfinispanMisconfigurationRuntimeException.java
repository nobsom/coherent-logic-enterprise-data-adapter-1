package com.coherentlogic.infinispan.cache.exceptions;

import org.springframework.core.NestedRuntimeException;

/**
 * An exception which is thrown when the Infinispan configuration is incorrect -- in this case this pertains to
 * expectations that this framework has insofar as Infinispan is concerned (eg. the reference to the Transaction Manager
 * is null).
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class InfinispanMisconfigurationRuntimeException extends NestedRuntimeException {

    private static final long serialVersionUID = 8682440588952517153L;

    public InfinispanMisconfigurationRuntimeException(String msg) {
        super(msg);
    }
}
