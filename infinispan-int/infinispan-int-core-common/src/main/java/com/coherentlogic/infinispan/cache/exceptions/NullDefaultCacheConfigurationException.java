package com.coherentlogic.infinispan.cache.exceptions;

import org.springframework.core.NestedRuntimeException;

/**
 * Thrown when the defaultCacheName was null
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class NullDefaultCacheConfigurationException extends NestedRuntimeException {

    private static final long serialVersionUID = 1640315544557848252L;

    public NullDefaultCacheConfigurationException () {
        super ("The defaultCacheConfiguration is null which means the defaultCacheName was null. This can be fixed by calling TBD");
    }
}
