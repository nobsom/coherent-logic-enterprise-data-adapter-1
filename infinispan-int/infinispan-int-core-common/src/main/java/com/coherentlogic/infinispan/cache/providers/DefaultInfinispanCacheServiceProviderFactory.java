package com.coherentlogic.infinispan.cache.providers;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.infinispan.Cache;
import org.infinispan.manager.EmbeddedCacheManager;

import org.infinispan.stats.CacheContainerStats;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.coherentlogic.coherent.data.adapter.core.cache.CacheServiceProviderSpecification;
import com.coherentlogic.coherent.data.adapter.core.factories.TypedFactory;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;

/**
 * A cache provider template implementation for the Infinispan shared-memory data grid.
 *
 * @see {@link SpringEmbeddedCacheManager} and {@link SpringEmbeddedCacheManager#getNativeCacheManager()}
 * @see import org.infinispan.spring.provider.SpringEmbeddedCacheManager;
 *
 * @see <a href="http://infinispan.org/docs/stable/user_guide/user_guide.html">Infinispan User Guide</a>
 * 
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class DefaultInfinispanCacheServiceProviderFactory
    implements TypedFactory<CacheServiceProviderSpecification<String>> {

    private static final Logger log = LoggerFactory.getLogger(DefaultInfinispanCacheServiceProviderFactory.class);

    private final String defaultCacheName;

    private final EmbeddedCacheManager cacheManager;

    public DefaultInfinispanCacheServiceProviderFactory(
        EmbeddedCacheManager cacheManager,
        String defaultCacheName
    ) {
        this.cacheManager = cacheManager;
        this.defaultCacheName = defaultCacheName;
    }

    /**
     * Method creates the {@link #cacheManager} using the GlobalConfiguration and Configuration instances.
     */
    @PostConstruct
    public void start () {

        log.info("start: method begins.");

        cacheManager.start();

        CacheContainerStats stats = cacheManager.getStats();

        log.info("start: method ends; started the cacheManager (stats: " + stats + ").");
    }

    @PreDestroy
    public void stop () {
        cacheManager.stop();
    }

    @Override
    public CacheServiceProviderSpecification<String> getInstance() {
        return getInstance(defaultCacheName);
    }

    public CacheServiceProviderSpecification<String> getInstance(String cacheName) {

        Cache<String, SerializableBean> cache = cacheManager.getCache(cacheName);

        return new DefaultInfinispanCacheServiceProvider (cache);
    }

//    public MapCompliantCacheServiceProvider<String> getInstance(String cacheName) {
//
//        // final SpringCache springCache = cacheManager.getCache(cacheName); // <String, SerializableBean>
//        final BasicCache<String, SerializableBean> basicCache = cacheManager.getCache(cacheName);
//
////        @SuppressWarnings("unchecked")
////        final BasicCache<String, SerializableBean> basicCache =
////            (BasicCache<String, SerializableBean>) springCache.getNativeCache();
//
//        return new MapCompliantCacheServiceProvider<String> (basicCache) {
//
//            @Override
//            public <V extends SerializableBean> void put(String key, V value) {
//
//                AdvancedCache<String, SerializableBean> advancedCache =
//                    ((Cache<String, SerializableBean>)basicCache).getAdvancedCache();
//
//                try {
//
//                    advancedCache.getTransactionManager().begin();
//
//                    lock(advancedCache, key);
//
//                    basicCache.put(key, value);
//
//                    unlock(advancedCache, key);
//
//                    advancedCache.getTransactionManager().commit();
//
//                } catch (
//                    SecurityException
//                    | IllegalStateException
//                    | NotSupportedException
//                    | SystemException
//                    | RollbackException
//                    | HeuristicMixedException
//                    | HeuristicRollbackException e
//                ) {
//                    throw new RuntimeException ("TX-related failed.", e);
//                }
//                
//                //unlock (advancedCache, key);
//            }
//
//            void lock (AdvancedCache<String, SerializableBean> advancedCache, String... keys) {
//                advancedCache.lock(keys);
//            }
//
//            void unlock (AdvancedCache<String, SerializableBean> advancedCache, String... keys) {
//
//                LockManager lockManager = advancedCache.getLockManager();
//
//                Object lockOwner = lockManager.getOwner(keys);
//
//                Collection<Object> keysCollection = Collections.<Object>singletonList(keys);
//
//                lockManager.unlock(keysCollection, lockOwner);
//            }
//        };
//    }
}

//public abstract class AbstractInfinispanCacheServiceProviderFactory
//implements TypedFactory<MapCompliantCacheServiceProvider<String>> {
//
//private static final Logger log = LoggerFactory.getLogger(
//    AbstractInfinispanCacheServiceProviderFactory.class);
//
//private final String defaultCacheName;
//
//private final SpringEmbeddedCacheManager cacheManager;
//
//public AbstractInfinispanCacheServiceProviderFactory(
//    SpringEmbeddedCacheManager cacheManager,
//    String defaultCacheName
//) {
//    this.cacheManager = cacheManager;
//    this.defaultCacheName = defaultCacheName;
//}
//
///**
// * Method returns an instance of {@link DefaultCacheManager} which is used for caching requests using the
// * URI as the key and the resultant XML as the value.
// *
// * For example:
// * <code><pre>@Override
// * protected DefaultCacheManager configure() {
// *
// *     GlobalConfigurationBuilder globalConfigurationBuilder =
// *         GlobalConfigurationBuilder.defaultClusteredBuilder();
// *
// *     GlobalConfiguration globalConfiguration =
// *         globalConfigurationBuilder
// *             .transport()
// *             .clusterName("FREDClientCluster")
// *             .defaultTransport()
// *             .defaultCacheName("FREDClientCache")
// *             .build();
// *
// *     ConfigurationBuilder configurationBuilder = new ConfigurationBuilder ();
// *
// *     Configuration configuration = configurationBuilder
// *         .jmxStatistics()
// *         .clustering()
// *             .cacheMode(CacheMode.DIST_SYNC)
// *             .l1()
// *             .lifespan(60000L * 5L) // Five minutes
// *         .build();
// *
// *     DefaultCacheManager result = new DefaultCacheManager(globalConfiguration, configuration);
// *
// *     result.addListener(new FREDClusterAndCacheListener ());
// *
// *     return result;
// * }</pre></code>
// *
// * For the above example it may be necessary to set the following run configuration VM arguments:
// *
// * -Djava.net.preferIPv4Stack=true -Djgroups.bind_addr=127.0.0.1
// */
////protected abstract DefaultCacheManager configure ();
//
///**
// * Method creates the {@link #cacheManager} using the GlobalConfiguration and Configuration instances.
// */
//@PostConstruct
//public void start () {
//
//    log.info("start: method begins.");
//
//    cacheManager.getNativeCacheManager().start();
//
//    CacheContainerStats stats = cacheManager.getNativeCacheManager().getStats();
//
//    log.info("start: method ends; started the cacheManager: " + stats);
//}
//
//@PreDestroy
//public void stop () {
//    cacheManager.stop();
//}
//
//@Override
//public MapCompliantCacheServiceProvider<String> getInstance() {
//    return getInstance(defaultCacheName);
//}
//
//public MapCompliantCacheServiceProvider<String> getInstance(String cacheName) {
//
//    final SpringCache springCache = cacheManager.getCache(cacheName); // <String, SerializableBean>
//
//    @SuppressWarnings("unchecked")
//    final BasicCache<String, SerializableBean> basicCache =
//        (BasicCache<String, SerializableBean>) springCache.getNativeCache();
//
//    return new MapCompliantCacheServiceProvider<String> (basicCache) {
//
//        @Override
//        public <V extends SerializableBean> void put(String key, V value) {
//
//            AdvancedCache<String, SerializableBean> advancedCache =
//                ((Cache<String, SerializableBean>)basicCache).getAdvancedCache();
//
//            //lock (advancedCache, key);
//
//            try {
//
//                advancedCache.getTransactionManager().begin();
//
//                lock(advancedCache, key);
//
//                basicCache.put(key, value);
//
//                unlock(advancedCache, key);
//
//                advancedCache.getTransactionManager().commit();
//
//            } catch (
//                SecurityException
//                | IllegalStateException
//                | NotSupportedException
//                | SystemException
//                | RollbackException
//                | HeuristicMixedException
//                | HeuristicRollbackException e
//            ) {
//                throw new RuntimeException ("TX-related failed.", e);
//            }
//            
//            //unlock (advancedCache, key);
//        }
//
//        void lock (AdvancedCache<String, SerializableBean> advancedCache, String... keys) {
//            advancedCache.lock(keys);
//        }
//
//        void unlock (AdvancedCache<String, SerializableBean> advancedCache, String... keys) {
//
//            LockManager lockManager = advancedCache.getLockManager();
//
//            Object lockOwner = lockManager.getOwner(keys);
//
//            Collection<Object> keysCollection = Collections.<Object>singletonList(keys);
//
//            lockManager.unlock(keysCollection, lockOwner);
//        }
//    };
//}
//}