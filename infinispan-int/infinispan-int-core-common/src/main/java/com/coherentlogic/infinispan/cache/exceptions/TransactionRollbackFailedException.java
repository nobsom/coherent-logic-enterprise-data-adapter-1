package com.coherentlogic.infinispan.cache.exceptions;

import org.springframework.core.NestedRuntimeException;

/**
 * Thrown when the put operation on the cache has failed; note the parent exception will contain the details
 * regarding what went wrong.
 */
public class TransactionRollbackFailedException extends NestedRuntimeException {

    private static final long serialVersionUID = -1692517106954635959L;

    private final Throwable originalCause;

    public TransactionRollbackFailedException(String msg, Throwable cause, Throwable originalCause) {

        super(msg, cause);

        this.originalCause = originalCause;
    }

    public Throwable getOriginalCause() {
        return originalCause;
    }
}
