<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" >
<xsl:output method="text" omit-xml-declaration="yes" indent="no"/>
<xsl:template match="/">
realtime_start,realtime_end,date,value
<xsl:for-each select="//observation">
    <xsl:value-of select="concat(@realtime_start,',',@realtime_end,',',@date,',',@value,'&#xA;')"/>
</xsl:for-each>
</xsl:template>
</xsl:stylesheet>