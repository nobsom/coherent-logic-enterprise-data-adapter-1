package com.coherentlogic.coherent.data.model.core.annotations;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.ReflectionUtils.FieldCallback;
import org.springframework.util.ReflectionUtils.FieldFilter;

import com.coherentlogic.coherent.data.model.core.annotations.Visitable;

/**
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class VisitableAnnotationProcessor {

    private static final Logger log = LoggerFactory.getLogger(VisitableAnnotationProcessor.class);

    private final Map<Class<?>, Set<Field>> fieldsMap = new HashMap<Class<?>, Set<Field>> ();

    protected Map<Class<?>, Set<Field>> getFieldsMap() {
        return fieldsMap;
    }

    /**
     * Used to register all classes in a package that are tagged with the {@link Visitable} annotation at the class-
     * -level.
     */
    public VisitableAnnotationProcessor register (String packageName) {

        register(Visitable.class, packageName);

        return this;
    }

    /**
     * Used to register all classes in a package that are tagged with the {@link Visitable} annotation at the class-
     * -level.
     */
    protected void register (Class<? extends Annotation> annotation, String packageName) {

        Reflections reflections = new Reflections(packageName);

        Set<Class<?>> annotatedClasses =  reflections.getTypesAnnotatedWith(annotation);

        annotatedClasses.forEach(
            annotatedClass -> {
                register(annotation, annotatedClass);
            }
        );
    }

    /**
     * Used to register specific classes that use the {@link Visitable} annotation to mark properties that will be
     * included in the parameters passed to the function that is executed when the forEachFieldPerBean or forAllFieldsPerBean
     * methods are called.
     */
    protected void register (Class<? extends Annotation> annotation, Class<?>... targets) {
        register (annotation, Arrays.asList(targets));
    }

    /**
     * Used to register classes that use the {@link Visitable} annotation to mark properties that will be included in
     * the parameters passed to the function that is executed when the forEachFieldPerBean or forAllFieldsPerBean
     * methods are called.
     */
    protected void register (Class<? extends Annotation> annotation, List<Class<?>> targets) {

        targets.forEach(
            target -> {

                log.debug("Registering the target: " + target);

                if (target != null) {

                    Set<Field> fields = new HashSet<Field> ();

                    ReflectionUtils.doWithFields(
                        target,
                        new FieldCallback(){

                            @Override
                            public void doWith(final Field field) throws IllegalArgumentException,
                                IllegalAccessException {

                                log.debug("Adding the field: " + field);

                                fields.add(field);
                            }
                        },
                        new FieldFilter() {

                            @Override
                            public boolean matches(final Field field) {
                                return field.isAnnotationPresent(Visitable.class);
                            }
                        }
                    );

                    fieldsMap.put(target, fields);
                }
            }
        );
    }

    public Set<Field> getFieldsFor(Class<?> target) {
        return fieldsMap.get(target);
    }
}
