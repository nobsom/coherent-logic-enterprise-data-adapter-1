package com.coherentlogic.coherent.data.model.core.strategies;

import java.util.Collection;
import java.util.function.Consumer;

import com.coherentlogic.coherent.data.model.core.domain.AcceptorSpecification;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;

/**
 * A specification for traversing a bean which is used by implementation of the {@link AcceptorSpecification}.
 *
 * There should be default implementations available for the following bean traversal strategies:
 *
 * pre depth first     visit the node prior to traversing its children.
 * post depth first    visit the node after traversing its children.
 * depth first around  visit the node and it's up to the node if/when to traverse its children.
 * breadth first       visit the nodes in breadth-first ordering.
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 *
 * @param <S> The type of SerializableBean.
 */
@FunctionalInterface
public interface TraversalStrategySpecification {

    /**
     * Method performs the traversal operation on the serializableBean.
     *
     * @param serializableBean The bean that will have the operation applied.
     *
     * @param visitors Will be called for the serializableBean and any beans it includes.
     */
    void execute (SerializableBean serializableBean, Collection<Consumer<SerializableBean>> visitors);
}
