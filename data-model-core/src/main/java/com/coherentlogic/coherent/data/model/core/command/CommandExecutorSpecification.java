package com.coherentlogic.coherent.data.model.core.command;

import java.util.function.Consumer;

import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;

/**
 * Specification for CommandExecutor implementations which allows for commands to be added and executed.
 * 
 * @TODO Only one implementation of this exists at the moment so we may consider removing the interface.
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public interface CommandExecutorSpecification {

//    <S extends SerializableBean> void addCommand (CommandSpecification... commands);
//
//    <S extends SerializableBean> void addCommand (Collection<CommandSpecification> commands);

//    private static class TestBean extends SerializableBean {
//
//                @Override
//                <S> void invoke(Consumer<S> consumer) {
//                    consumer.accept((S) this);
//                }
//            };

    /**
     * Experimental at the moment -- implementation of the command pattern.
     * @param consumer
     */
    <S extends SerializableBean> void invoke(Consumer<S> consumer);
}
