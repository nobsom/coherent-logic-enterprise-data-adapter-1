package com.coherentlogic.coherent.data.model.core.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Used as a marker for any property that should be included when the function is passed to any of the
 * {@link AbstractQueryBuilder} <i>doAccept</i> methods.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.FIELD})
public @interface Visitable {

}
