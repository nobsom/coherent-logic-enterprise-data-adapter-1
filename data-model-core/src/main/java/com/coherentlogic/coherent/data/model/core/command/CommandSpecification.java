package com.coherentlogic.coherent.data.model.core.command;

import java.io.Serializable;

import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;

/**
 * A command interface that is executed on a specific {@link SerializableBean}. The command is executed after
 * the {@link SerializableBean} has been instantiated and populated with data.
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 *
 * @param <S> The type of {@link SerializableBean}.
 */
@FunctionalInterface
public interface CommandSpecification extends Serializable {

    /**
     * @TODO: See if this method can be changed as we have below, however this is causing problems right now and so I'm
     *        reverting to a working solution.
     *
     * <S extends SerializableBean> void execute (S serializableBean);
     */
    void execute (SerializableBean serializableBean);
}
