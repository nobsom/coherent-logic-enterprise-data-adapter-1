package com.coherentlogic.coherent.data.model.core.strategies;

import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;

/**
 * @todo Implementation details needed.
 *
 */
public class PreDepthFirstTraversalStrategy implements StrategySpecification<SerializableBean> {

    private static final long serialVersionUID = 7164586273231814624L;

    @Override
    public void execute(SerializableBean target) {
        throw new RuntimeException ("Method not implemented.");
    }
}
