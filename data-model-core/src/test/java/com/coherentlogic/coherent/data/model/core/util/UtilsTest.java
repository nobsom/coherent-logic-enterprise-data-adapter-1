package com.coherentlogic.coherent.data.model.core.util;

import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

/**
 * Unit test for the {@link Utils} class.
 *
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class UtilsTest {

    public void testAssertNotNullThrowsNPE() {
    	assertThrows (NullPointerException.class, () -> { Utils.assertNotNull("isNull", null); });
    }

    @Test
    public void testAssertNotNullDoesNotThrowNPE() {
        Utils.assertNotNull("isNotNull", "is not null");
    }
}
