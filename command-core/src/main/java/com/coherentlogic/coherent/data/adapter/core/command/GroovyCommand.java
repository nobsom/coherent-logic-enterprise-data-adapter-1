package com.coherentlogic.coherent.data.adapter.core.command;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

//import com.coherentlogic.coherent.data.adapter.core.builders.GroovyCommand;
//import com.coherentlogic.coherent.data.adapter.core.builders.AbstractQueryBuilderTest.ExampleQueryBuilder;

public class GroovyCommand<X, Y> extends ScriptedCommand<X, Y> {

    public GroovyCommand () {
        this (new ScriptEngineManager().getEngineByName("groovy"));
    }

    public GroovyCommand (ClassLoader classLoader) {
        this (new ScriptEngineManager(classLoader).getEngineByName("groovy"));
    }

    public GroovyCommand(ScriptEngine scriptEngine) {
        super(scriptEngine);
    }
    
//    @Test
//    public void testInvokeWithGroovyCommand () throws InterruptedException, ExecutionException {
//
//        Future<Integer> result =
//            new ExampleQueryBuilder ().invoke(
//                new GroovyCommand<ExampleQueryBuilder, Integer> ().applying(
//                    "return 777"
//                )
//            );
//
//        assertEquals (new Integer (777), result.get());
//    }
}
