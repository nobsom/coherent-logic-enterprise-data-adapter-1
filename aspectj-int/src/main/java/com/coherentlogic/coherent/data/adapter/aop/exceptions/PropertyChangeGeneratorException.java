package com.coherentlogic.coherent.data.adapter.aop.exceptions;

import org.springframework.core.NestedRuntimeException;

/**
 * Thrown when the an exception is thrown within the {@link DefaultPropertyChangeGenerator}.
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class PropertyChangeGeneratorException extends NestedRuntimeException {

    private static final long serialVersionUID = -2504265172653939223L;

    public PropertyChangeGeneratorException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public PropertyChangeGeneratorException(String msg) {
        super(msg);
    }
}
