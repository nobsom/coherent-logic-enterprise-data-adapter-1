package com.coherentlogic.coherent.data.model.core.domain;

import org.aspectj.lang.annotation.Aspect;

@Aspect
public class PropertyChangeEventGeneratorAspect<T> extends PropertyChangeEventGeneratorMethodInterceptor<T> {

    public PropertyChangeEventGeneratorAspect(Class<T> targetClass) {
        super(targetClass);
    }
}
